<%-- 
    Document   : GestionarU
    Created on : 21/11/2019, 05:02:51 PM
    Author     : gregr
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800">Usuarios</h1>
    <p class="mb-4">Información general de los usuarios registrados en el sistema.</p>
    <div class="card shadow mb-4">
        <div class="card-header py-3 bg-gray-800">
            <h6 class="m-0 font-weight-bold text-white" style="text-align: center">Usuarios</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr>
                            <th style="text-align: center;">Nombre</th>                            
                            <th style="text-align: center;">Tipo</th>                            
                            <th style="text-align: center;">Acciones</th>
                        </tr>
                    </thead>                  
                    <tbody>  
                        <c:forEach var="datos" items="${usuarios}">
                        <tr>
                            <td style="text-align: center;">${datos.nombre}</td>                                                        
                            <td style="text-align: center;">
                                <c:choose>
                                    <c:when test="${datos.tipo eq 1}">Administrador</c:when>
                                    <c:when test="${datos.tipo eq 2}">Usuario normal</c:when>
                                </c:choose>  
                            </td>                        
                            <td style="text-align: center;">
                                <a title="Información" class="btn btn-info btn-circle" onclick="location.href='editar-usuario?id=${datos.id}'">
                                    <i class="fas fa-info" style="color: white"></i>
                                </a>
                                <a title="Inhabilitar" class="btn btn-danger btn-circle estatus-u" data-id="${datos.id}">
                                        <i class="fas fa-trash" style="color: white"></i>
                                </a>                                
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>    
</div>

<script src="vendor/jquery/jquery.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function (){
        $(".estatus-u").click(function (){
            idUser = $(this).attr("data-id");
            value = confirm("¿Desea eliminar este usuario?");           
            if (value) {
                $.ajax({
                    url: "eliminar-usuario",
                    type: "POST",
                    data : {
                        idUser : idUser
                    },
                    success: function (data, textStatus, jqXHR) {
                        location.reload();
                    }               
                });
            }
        }); 
    });           
</script>


