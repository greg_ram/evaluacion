/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import Conexion.Conexion;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.dto.Empleado;

/**
 *
 * @author usuario
 */
public class EmpleadoD {
    
    private Conexion cbd = new Conexion();
    
    public Empleado create(Empleado e) throws SQLException {
        String sql = "INSERT INTO empleado (nombre, fecha_nacimiento, email, genero, telefono, celular, fecha_ingreso, id_departamento, id_empresa) VALUES (?,?,?,?,?,?,?,?,?) ";
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ps.setString(1, e.getNombre());
        ps.setDate(2, e.getFecha());
        ps.setString(3, e.getEmail());
        ps.setString(4, e.getGenero());
        ps.setString(5, e.getTelefono());
        ps.setString(6, e.getCelular());
        ps.setDate(7, e.getFechai());  
        ps.setInt(8, e.getDepartamentoId());
        ps.setInt(9, e.getEmpresaId());
        ps.executeUpdate();
                        
        return new Empleado();
    }
    
    public List<Empleado> findAll() throws SQLException {
        String sql = "SELECT * FROM empleado";
        
        List<Empleado> empleados = new ArrayList<>();
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        Empleado e = null;
        while (rs.next()) {            
            e = new Empleado();
            e.setId(rs.getInt("id"));
            e.setNombre(rs.getString("nombre"));
            e.setFecha(rs.getDate("fecha_nacimiento"));
            e.setEmail(rs.getString("email"));
            e.setGenero(rs.getString("genero"));            
            e.setTelefono(rs.getString("telefono"));
            e.setCelular(rs.getString("celular"));
            e.setFechai(rs.getDate("fecha_ingreso")); 
            e.setEmpresa(new EmpresaD().findById(rs.getInt("id_empresa")));
            e.setDepartamento(new DepartamentoD().findById(rs.getInt("id_departamento")));
            empleados.add(e);            
        }        
        return empleados;
    }
    
    public List<Empleado> findAllByE(Integer id) throws SQLException {
        String sql = "SELECT * FROM empleado Where id_empresa = ?";
        
        List<Empleado> empleados = new ArrayList<>();
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        Empleado e = null;
        while (rs.next()) {            
            e = new Empleado();
            e.setId(rs.getInt("id"));
            e.setNombre(rs.getString("nombre"));
            e.setFecha(rs.getDate("fecha_nacimiento"));
            e.setEmail(rs.getString("email"));
            e.setGenero(rs.getString("genero"));            
            e.setTelefono(rs.getString("telefono"));
            e.setCelular(rs.getString("celular"));
            e.setFechai(rs.getDate("fecha_ingreso")); 
            e.setEmpresa(new EmpresaD().findById(rs.getInt("id_empresa")));
            e.setDepartamento(new DepartamentoD().findById(rs.getInt("id_departamento")));
            empleados.add(e);            
        }        
        return empleados;
    }
    
    public List<Empleado> findAllByD(Integer id) throws SQLException {
        String sql = "SELECT * FROM empleado Where id_departamento = ?";
        
        List<Empleado> empleados = new ArrayList<>();
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        Empleado e = null;
        while (rs.next()) {            
            e = new Empleado();
            e.setId(rs.getInt("id"));
            e.setNombre(rs.getString("nombre"));
            e.setFecha(rs.getDate("fecha_nacimiento"));
            e.setEmail(rs.getString("email"));
            e.setGenero(rs.getString("genero"));            
            e.setTelefono(rs.getString("telefono"));
            e.setCelular(rs.getString("celular"));
            e.setFechai(rs.getDate("fecha_ingreso")); 
            e.setEmpresa(new EmpresaD().findById(rs.getInt("id_empresa")));
            e.setDepartamento(new DepartamentoD().findById(rs.getInt("id_departamento")));
            empleados.add(e);            
        }        
        return empleados;
    }
    
    public List<Empleado> findAllByED(Integer idE, Integer idD) throws SQLException {
        String sql = "SELECT * FROM empleado Where id_empresa = ? And id_departamento = ?";
        
        List<Empleado> empleados = new ArrayList<>();
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ps.setInt(1, idE);
        ps.setInt(2, idD);
        ResultSet rs = ps.executeQuery();
        Empleado e = null;
        while (rs.next()) {            
            e = new Empleado();
            e.setId(rs.getInt("id"));
            e.setNombre(rs.getString("nombre"));
            e.setFecha(rs.getDate("fecha_nacimiento"));
            e.setEmail(rs.getString("email"));
            e.setGenero(rs.getString("genero"));            
            e.setTelefono(rs.getString("telefono"));
            e.setCelular(rs.getString("celular"));
            e.setFechai(rs.getDate("fecha_ingreso")); 
            e.setEmpresa(new EmpresaD().findById(rs.getInt("id_empresa")));
            e.setDepartamento(new DepartamentoD().findById(rs.getInt("id_departamento")));
            empleados.add(e);            
        }
        
        return empleados;
    }
    
    public Empleado findById(Integer id) throws SQLException {
        String sql = "SELECT * FROM empleado WHERE id = ?";
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        Empleado e = null;
        while (rs.next()) {
            e = new Empleado();
            e.setId(rs.getInt("id"));
            e.setNombre(rs.getString("nombre"));
            e.setFecha(rs.getDate("fecha_nacimiento"));
            e.setEmail(rs.getString("email"));
            e.setGenero(rs.getString("genero"));            
            e.setTelefono(rs.getString("telefono"));
            e.setCelular(rs.getString("celular"));
            e.setFechai(rs.getDate("fecha_ingreso")); 
            e.setEmpresa(new EmpresaD().findById(rs.getInt("id_empresa")));
            e.setDepartamento(new DepartamentoD().findById(rs.getInt("id_departamento")));
            e.sethEntrada(rs.getTime("h_entrada"));
            e.sethSalida(rs.getTime("h_salida"));
        }
        
        return e;
    }
    
    public Empleado update(Empleado e) throws SQLException {
        String sql = "Update empleado set nombre = ?, fecha_nacimiento = ?, email = ?, "
                + "genero = ?, telefono = ?, celular = ?, id_departamento = ?, id_empresa = ? "
                + "Where id = ?";
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ps.setString(1, e.getNombre());
        ps.setDate(2, e.getFecha());
        ps.setString(3, e.getEmail());
        ps.setString(4, e.getGenero());
        ps.setString(5, e.getTelefono());
        ps.setString(6, e.getCelular());        
        ps.setInt(7, e.getDepartamentoId());
        ps.setInt(8, e.getEmpresaId());
        ps.setInt(9, e.getId());
        ps.executeUpdate();
        
        return new Empleado();
        
    }
    
    public Empleado Horarios(Empleado e) throws SQLException {
        String sql = "Update empleado set h_entrada = ?, h_salida = ? Where id = ?";
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ps.setTime(1, e.gethEntrada());
        ps.setTime(2, e.gethSalida());
        ps.setInt(3, e.getId());
        ps.executeUpdate();
        
        return new Empleado();
        
    }
    
    public Empleado delete(Integer id) throws SQLException {
        String sql = "Delete From empleado Where id = ?";
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ps.setInt(1, id);
        ps.executeUpdate();
        
        return new Empleado();
    }
    
}
