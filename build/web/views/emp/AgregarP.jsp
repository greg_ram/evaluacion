<%-- 
    Document   : AgregarP
    Created on : 21/11/2019, 05:03:37 PM
    Author     : gregr
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container-fluid">
    <c:if test="${info}">
        <button type="button" onclick="location.href='gestionar-empleado'" class="btn btn-outline-secondary">
            &larr; Atrás
        </button>
        <hr/>
    </c:if>
    <div class="card shadow mb-4">
        <div class="card-header py-3 bg-gray-900">
            <h6 class="m-0 font-weight-bold text-white" style="text-align: center">${title}</h6>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="p-2">
                        <form id="form" action="${action}" method="POST">
                            <input type="hidden" value="${empleado.id}" name="idEmp"/>
                            <div class="form-group row">
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" id="nombre" name="nombre"
                                           placeholder="Nombre" autocomplete="off" value="${empleado.nombre}" required />
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" value="${empleado.fecha}" id="fecha" name="fecha"
                                           placeholder="Fecha de nacimiento" autocomplete="off" onfocus="(this.type = 'date')" required />
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <input id="email" name="email" class="form-control" type="email" 
                                           placeholder="Correo electrónico" value="${empleado.email}" autocomplete="off" required />
                                </div>                                                                 
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4 col-xs-12">
                                    <c:if test="${empleado.genero != null}"><input type="hidden" value="${empleado.genero}" name="idProv"/></c:if>
                                    <select id="genero" name="genero" class="form-control">
                                        <option value="" <c:if test="${empleado.genero eq ''}">selected</c:if>>Genero:</option>
                                        <option value="M"<c:if test="${empleado.genero eq 'M'}">selected</c:if> >Femenino</option>
                                        <option value="H"<c:if test="${empleado.genero eq 'H'}">selected</c:if>>Masculino</option>                                        
                                    </select>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" id="telefono" name="telefono"
                                           placeholder="Teléfono" maxlength="10" value="${empleado.telefono}" autocomplete="off" />
                                </div> 
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" id="celular" name="celular"
                                           placeholder="Celular" maxlength="10" value="${empleado.celular}" autocomplete="off" />
                                </div>                                 
                            </div>                                
                            <div class="form-group row">
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" id="fecha_i" name="fecha_i" <c:if test="${empleado.id != null}">disabled</c:if> value="${empleado.fechai}"
                                           placeholder="Fecha de Ingreso" autocomplete="off" onfocus="(this.type = 'date')" required />
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <c:if test="${empleado.empresa.id != null}"><input type="hidden" value="${empleado.empresa.id}"/></c:if>
                                    <select id="idEmpr" name="idEmpr" class="form-control">
                                        <option value="">Seleccione una empresa</option>
                                        <c:forEach var="values" items="${empresas}">
                                            <option value="${values.id}" <c:if test="${values.id eq empleado.empresa.id}">selected</c:if> >${values.nombre}</option>
                                        </c:forEach>                                    
                                    </select>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <c:if test="${empleado.departamento.id != null}"><input type="hidden" value="${empleado.departamento.id}"/></c:if>
                                    <select id="idDep" name="idDep" class="form-control" >
                                        <option value="">Seleccione un departamento</option>
                                        <c:forEach var="values" items="${departamentos}">
                                            <option value="${values.id}" <c:if test="${values.id eq empleado.departamento.id}">selected</c:if> >${values.nombre}</option>
                                        </c:forEach>                                    
                                    </select>
                                </div>
                            </div>
                            <hr />                               
                            <div style="text-align: right">
                                <button class="btn btn-primary btn-user" type="submit">Guardar</button>
                            </div>  
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>

