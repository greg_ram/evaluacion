<%-- 
    Document   : AgregarP
    Created on : 21/11/2019, 05:03:37 PM
    Author     : gregr
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container-fluid">
    <c:if test="${contacto}">
        <button type="button" onclick="location.href='gestionar-proveedor'" class="btn btn-outline-secondary">
            &larr; Atrás
        </button>
        <hr/>
    </c:if>
    <div class="card shadow mb-4">
        <div class="card-header py-3 bg-gray-900">
            <h6 class="m-0 font-weight-bold text-white" style="text-align: center">${title}</h6>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="p-2">
                        <form id="form" action="${action}" method="POST">
                            <input type="hidden" value="${prov.idProveedor}" name="idProv"/>
                            <div class="form-group row">
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" id="nombre" name="nombre"
                                           placeholder="Nombre" autocomplete="off" value="${prov.nombreProveedor}" required />
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" id="fecha" name="fecha"
                                           placeholder="Fecha de nacimiento" autocomplete="off" onfocus="(this.type = 'date')" required />
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <input id="email" name="email" class="form-control" type="email" 
                                           placeholder="Correo electrónico" value="${prov.correo}" autocomplete="off" required />
                                </div>                                                                 
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4 col-xs-12">
                                    <c:if test="${idProv != null}"><input type="hidden" value="${idProv}" name="idProv"/></c:if>
                                    <select id="idProv" name="idProv" class="form-control" required <c:if test="${idProv != null}">disabled</c:if> >
                                        <option value="">Genero:</option>
                                        <option value="H" <c:if test="${values.idProveedor eq idProv}">selected</c:if> >${values.nombreProveedor}</option>
                                        <c:forEach var="values" items="${provs}">
                                            <option value="${values.idProveedor}" <c:if test="${values.idProveedor eq idProv}">selected</c:if> >${values.nombreProveedor}</option>
                                        </c:forEach>                                    
                                    </select>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" id="telefono" name="telefono"
                                           placeholder="Teléfono" maxlength="10" value="${prov.telefono}" autocomplete="off" required />
                                </div> 
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" id="celular" name="celular"
                                           placeholder="Celeluar" maxlength="10" value="${prov.telefono}" autocomplete="off" required />
                                </div>                                 
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" id="fecha_i" name="fecha_i"
                                           placeholder="Fecha de Ingreso" autocomplete="off" onfocus="(this.type = 'date')" required />
                                </div>
                            </div>
                            <hr />                               
                            <div style="text-align: right">
                                <button class="btn btn-primary btn-user" type="submit">Guardar</button>
                            </div>  
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <c:if test="${contacto}">
        <div style="text-align: right">
            <a onclick="location.href='contacto-proveedor?idProv=${prov.idProveedor}'" class="btn btn-success btn-icon-split">
                <span class="icon text-white-50">
                    <i class="fas fa-check" style="color: white"></i>
                </span>
                <span class="text" style="color: white">Registrar nuevo contacto.</span>
            </a>
            <hr/>
        </div>
        <div class="card shadow mb-4">
            <div class="card-header py-3 bg-gray-900">
                <h6 class="m-0 font-weight-bold text-white" style="text-align: center">Contactos del proveedor</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead class="thead-dark">
                            <tr>
                                <th style="text-align: center;">Nombre</th>
                                <th style="text-align: center;">Teléfono</th>
                                <th style="text-align: center;">Extensión</th>
                                <th style="text-align: center;">Celular</th>
                                <th style="text-align: center;">Correo</th>
                                <th style="text-align: center;">Acciones</th>

                            </tr>
                        </thead>                  
                        <tbody>
                            <c:forEach var="datos" items="${contactos}">
                                <c:if test="${prov.idProveedor eq datos.idProveedor.idProveedor}">
                                    <tr>
                                        <td style="text-align: center;">${datos.nombreContacto}</td>
                                        <td style="text-align: center;">${datos.telefono}</td>
                                        <td style="text-align: center;">${datos.extension}</td>
                                        <td style="text-align: center;">${datos.celular}</td>
                                        <td style="text-align: center;">${datos.correo}</td>
                                        <td style="text-align: center;">
                                            <a onclick="location.href='editar-contacto?idCon=${datos.idContacto}'" class="btn btn-info btn-circle">
                                                <i class="fas fa-info" style="color: white"></i>
                                            </a>
                                            <a class="btn btn-danger btn-circle">
                                                <i class="fas fa-trash" style="color: white"></i>
                                            </a>                        
                                        </td>
                                    </tr>
                                </c:if>
                            </c:forEach>                                                                                                                
                        </tbody>
                    </table>
                </div>
            </div>
        </div>    
    </c:if>
</div>

