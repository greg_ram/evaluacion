<%-- 
    Document   : GestionarP
    Created on : 22/11/2019, 06:24:29 PM
    Author     : gregr
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800">Empleados</h1>
    <p class="mb-4">Información general de los empleados registrados en el sistema.</p>
    <div class="card shadow mb-4">
        <div class="card-header py-3 bg-gray-800">
            <h6 class="m-0 font-weight-bold text-white" style="text-align: center">Empleados</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr>
                            <th style="text-align: center;">Nombre</th>
                            <th style="text-align: center;">Empresa</th>
                            <th style="text-align: center;">Departamento</th>
                            <!--<th style="text-align: center;">RFC</th>
                            <th style="text-align: center;">Estatus</th>
                            <th style="text-align: center;">Acciones</th>-->
                        </tr>
                    </thead>                  
                    <tbody>                    
                        <c:forEach var="datos" items="${proveedores}">
                            <tr>
                                <td style="text-align: center;">${datos.nombreProveedor}</td>
                                <td style="text-align: center;">${datos.correo}</td>
                                <td style="text-align: center;">${datos.telefono}</td>
                                <td style="text-align: center;">${datos.rfc}</td>                        
                                <td style="text-align: center;">
                                    <c:choose>
                                        <c:when test="${datos.estatus eq 1}">Activo</c:when>
                                        <c:when test="${datos.estatus eq 0}">Inactivo</c:when>
                                    </c:choose>
                                </td>                        
                                <td style="text-align: center;">
                                    <a onclick="location.href='editar-proveedor?idProv=${datos.idProveedor}'" class="btn btn-info btn-circle">
                                        <i class="fas fa-info" style="color: white"></i>
                                    </a>
                                    <c:if test="${datos.estatus eq 1}">
                                        <a title="Inhabilitar" class="btn btn-danger btn-circle estatus-prov" data-id="${datos.idProveedor}">
                                            <i class="fas fa-trash" style="color: white"></i>
                                        </a>
                                    </c:if>
                                    <c:if test="${datos.estatus eq 0}">
                                        <a title="Habilitar" class="btn btn-success btn-circle estatus-prov" data-id="${datos.idProveedor}">  
                                            <i class="fas fa-check" style="color: white"></i>
                                        </a>
                                    </c:if>                        
                                </td>
                            </tr>  

                        </c:forEach>

                    </tbody>
                </table>
            </div>
        </div>
    </div>    
</div>

<script src="vendor/jquery/jquery.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function (){
        $(".estatus-prov").click(function (){
            idProv = $(this).attr("data-id");
            value = confirm("¿Desea cambiar el estatus de este proveedor?");           
            if (value) {
                $.ajax({
                    url: "estatus-proveedor",
                    type: "POST",
                    data : {
                        idProv : idProv
                    },
                    success: function (data, textStatus, jqXHR) {
                        location.reload();
                    }               
                });
            }
        }); 
    });           
</script>