/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author gregr
 */
@WebServlet(name = "CtrlInicio", urlPatterns = {"/inicio"})
public class CtrlInicio extends HttpServlet {
    
    private HttpSession session;
    
    String url = "";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        this.session = request.getSession();

        url = request.getServletPath();
        
        if (session.getAttribute("tipo") != null) {
            if (url.equals("/inicio")) {
                request.setAttribute("body", "/views/Container.jsp");
                request.setAttribute("tipo", session.getAttribute("tipo"));
                getServletContext().getRequestDispatcher("/views/includes/MainInclude.jsp").forward(request, response);
            }                
                    
        } else {
            response.sendRedirect("/Usuarios/login");
        }
        
        
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

}
