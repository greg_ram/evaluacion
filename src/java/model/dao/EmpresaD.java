/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import Conexion.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.dto.Empresa;

/**
 *
 * @author usuario
 */
public class EmpresaD {
    
    private Conexion cbd = new Conexion();
    
    public List<Empresa> findAll() throws SQLException {
        String sql = "SELECT * FROM empresa where id != 4";
                        
        List<Empresa> empresas = new ArrayList<>();
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        Empresa e = null;
        while (rs.next()) {            
            e = new Empresa();
            e.setId(rs.getInt("id"));
            e.setNombre(rs.getString("nombre"));
            empresas.add(e);
            
        }
                
        return empresas;
    }
    
    public Empresa findById(Integer id) throws SQLException {
        String sql = "SELECT * FROM empresa WHERE id = ?";
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        Empresa e = null;
        while (rs.next()) {
            e = new Empresa();
            e.setId(rs.getInt("id"));
            e.setNombre(rs.getString("nombre"));
        }
        
        return e;
    }
    
}
