/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import Conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.dto.Usuario;


public class UsuarioD {
    
    private Conexion cbd;
    
    public UsuarioD() {
        cbd = new Conexion();
    }
    
    public Usuario login(Usuario u) throws SQLException {
        String sql = "SELECT * FROM usuario WHERE nombre = ? AND password = ?";
        System.out.println(sql);
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ps.setString(1, u.getNombre());
        ps.setString(2, u.getPassword());
        ResultSet rs = ps.executeQuery();
        Usuario ul = null;
        while (rs.next()) {
            ul = new Usuario();
            ul.setNombre(rs.getString("nombre"));
            ul.setPassword(rs.getString("password"));
            ul.setTipo(rs.getInt("tipo"));
        }
        
        return ul;
        
    }
    
    public Usuario findById(Integer id) throws SQLException {
        String sql = "SELECT * FROM usuario WHERE id = ?";
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        Usuario ul = null;
        while (rs.next()) {
            ul = new Usuario();
            ul.setId(rs.getInt("id"));
            ul.setNombre(rs.getString("nombre"));
            ul.setPassword(rs.getString("password"));
            ul.setTipo(rs.getInt("tipo"));
        }
        
        return ul;
        
    }
    
    public List<Usuario> findAll() throws SQLException {
        String sql = "SELECT * FROM usuario";
        
        List<Usuario> usuarios = new ArrayList<>();
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        Usuario u = null;
        while (rs.next()) {            
            u = new Usuario();
            u.setId(rs.getInt("id"));
            u.setNombre(rs.getString("nombre"));
            u.setPassword(rs.getString("password"));
            u.setTipo(rs.getInt("tipo"));
            usuarios.add(u);
            
        }
        
        return usuarios;
    }
    
    public Usuario create(Usuario e) throws SQLException {
        String sql = "INSERT INTO usuario (nombre, password, tipo) VALUES (?,?,?) ";
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ps.setString(1, e.getNombre());
        ps.setString(2, e.getPassword());
        ps.setInt(3, e.getTipo());        
        ps.executeUpdate();
        
        return new Usuario();
    }
    
    public Usuario update(Usuario e) throws SQLException {
        String sql = "Update usuario set nombre = ?, password = ?, tipo = ? Where id = ?";
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ps.setString(1, e.getNombre());
        ps.setString(2, e.getPassword());
        ps.setInt(3, e.getTipo()); 
        ps.setInt(4, e.getId());
        ps.executeUpdate();
        
        return new Usuario();
    }
    
    public Usuario delete(Integer id) throws SQLException {
        String sql = "Delete From usuario Where id = ?";
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ps.setInt(1, id);
        ps.executeUpdate();
        
        return new Usuario();
    }
    
}
