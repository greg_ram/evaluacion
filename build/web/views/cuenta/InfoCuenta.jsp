<%-- 
    Document   : InfoCuenta
    Created on : 22/11/2019, 09:03:55 PM
    Author     : gregr
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container-fluid">
    <button type="button" onclick="location.href='cuenta-proveedor?idProv=${cuenta.idProveedor.idProveedor}'" class="btn btn-outline-secondary">
        &larr; Atrás
    </button>
    <hr/>
    <div class="card shadow mb-4">
        <div class="card-header py-3 bg-gray-800">
            <h6 class="m-0 font-weight-bold text-white" style="text-align: center">Información de la cuenta</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="p-2">
                        <form id="form" action="#" method="POST">
                            <div class="form-group row">
                                <div class="col-sm-4 col-xs-12">
                                    <label>Proveedor</label>
                                    <input type="hidden" value="${cuenta.idProveedor.idProveedor}" name="idProv">
                                    <select id="idProv" class="form-control" required disabled="1">
                                        <option value="${cuenta.idProveedor.idProveedor}" selected>${cuenta.idProveedor.nombreProveedor}</option>                                                
                                    </select>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <label>Fecha de factura</label>
                                    <input type="text" class="form-control" id="fecha" name="fecha"
                                           placeholder="Fecha de facturación" value="${cuenta.fechaFactura}" autocomplete="off" required readonly="1"/>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <label>No. Factura</label>
                                    <input id="email" name="factura" class="form-control" type="factura" 
                                           placeholder="No. Factura" value="${cuenta.noFactura}" autocomplete="off" required readonly="1"/>
                                </div>                              
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4 col-xs-12">
                                    <label>Tipo de adquisición</label>
                                    <input type="hidden" value="${cuenta.idAdquisicion.idAdquisicion}" name="idAdq"/>
                                    <select name="idAdq" class="form-control" required disabled="1">
                                        <option value="${cuenta.idAdquisicion.idAdquisicion}" selected>${cuenta.idAdquisicion.tpAdquisicion}</option>
                                    </select>                                        
                                </div>     
                                <div class="col-sm-4 col-xs-12">
                                    <label>Label</label>
                                    <input type="text" class="form-control" id="desc" name="desc"
                                           placeholder="Descripción" autocomplete="off" value="${cuenta.descripcion}" readonly="1" required />
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <label>Camión (placas)</label>
                                    <input type="hidden" name="camion" value="${cuenta.idCamion.idCamion}"/>
                                    <select name="camion" class="form-control" disabled="1">
                                        <option value="${cuenta.idCamion.idCamion}">${cuenta.idCamion.placas}</option>
                                    </select>                                      
                                </div>                 
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4 col-xs-12">
                                    <label>Monto total</label>
                                    <input type="text" class="form-control" id="monto" name="monto"
                                           placeholder="Monto" value="${cuenta.monto}" readonly="1" autocomplete="off" required />                                        
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <label>Adeudo</label>
                                    <input type="text" class="form-control" name="adeudo" 
                                           placeholder="Adeudo" autocomplete="off" value="${cuenta.adeudo}" readonly="1" required/>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <label>Fecha de vencimiento</label>
                                    <input type="text" class="form-control" name="fecha_v" 
                                           placeholder="Fecha de vencimiento"
                                           autocomplete="off" value="${cuenta.fechaVencimiento}" readonly="1" required/>
                                </div>    
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4 col-xs-12">
                                    <label>Estatus</label>
                                    <input type="text" class="form-control" name="estatus"
                                                          value="${cuenta.estatus}" readonly="1" autocomplete="off" required />                                        
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div style="text-align: right">
        <a onclick="location.href='registrar-pago?cuenta=${cuenta.idCuenta}'" class="btn btn-success btn-icon-split">
            <span class="icon text-white-50">
                <i class="fas fa-check" style="color: white"></i>
            </span>
            <span class="text" style="color: white">Registrar nuevo pago.</span>
        </a>
        <hr/>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3 bg-gray-800">
            <h6 class="m-0 font-weight-bold text-white" style="text-align: center">Pagos realizados</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr>
                            <th style="text-align: center;">Monto de pago</th>
                            <th style="text-align: center;">Fecha de pago</th>
                            <th style="text-align: center;">Tipo de pago</th>
                        </tr>
                    </thead>                  
                    <tbody>
                        <c:forEach var="datos" items="${pagos}">
                            <c:if test="${datos.idCuenta.idCuenta eq cuenta.idCuenta}">
                                <tr>
                                    <td style="text-align: center;">${datos.pago}</td>
                                    <td style="text-align: center;">${datos.fechaPago}</td>
                                    <td style="text-align: center;">${datos.idTp.idTp}</td>
                                </tr>  
                            </c:if>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>    
</div>