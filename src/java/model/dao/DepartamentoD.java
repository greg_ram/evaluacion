/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model.dao;

import Conexion.Conexion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.dto.Departamento;

/**
 *
 * @author usuario
 */
public class DepartamentoD {
    
    private Conexion cbd = new Conexion();
    
    public List<Departamento> findAll() throws SQLException {
        String sql = "SELECT * FROM departamento Where id != 5";
        
        List<Departamento> departamentos = new ArrayList<>();
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        Departamento d = null;
        while (rs.next()) {            
            d = new Departamento();
            d.setId(rs.getInt("id"));
            d.setNombre(rs.getString("nombre"));
            departamentos.add(d);
            
        }        
        return departamentos;
    }
    
    public Departamento findById(Integer id) throws SQLException {
        String sql = "SELECT * FROM departamento WHERE id = ?";
        PreparedStatement ps = cbd.getCon().prepareStatement(sql);
        ps.setInt(1, id);
        ResultSet rs = ps.executeQuery();
        Departamento d = null;
        while (rs.next()) {
            d = new Departamento();
            d.setId(rs.getInt("id"));
            d.setNombre(rs.getString("nombre"));
        }
        
        return d;
    }
    
}
