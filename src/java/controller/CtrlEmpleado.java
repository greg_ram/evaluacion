/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.dao.DepartamentoD;
import model.dao.EmpleadoD;
import model.dao.EmpresaD;
import model.dto.Departamento;
import model.dto.Empleado;
import model.dto.Empresa;

/**
 *
 * @author gregr
 */
@WebServlet(name = "CtrlEmpleado", urlPatterns = {"/registrar-empleado", "/gestionar-empleado", "/editar-empleado", "/eliminar-empleado", "/horario-empleado"})
public class CtrlEmpleado extends HttpServlet {
    
    private HttpSession session;
    private EmpleadoD empleadoD;
    private EmpresaD empresaD;
    private DepartamentoD departamentoD;
        
    String url = "";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        this.session = request.getSession();
        
        url = request.getServletPath();
        Empleado empleado;
        
        if (session.getAttribute("tipo") != null) {
            switch (url) {
                case "/registrar-empleado":
                    if (empresaD == null) {
                        empresaD = new EmpresaD();
                    }
                    
                    if (departamentoD == null) {
                        departamentoD = new DepartamentoD();
                    }
                    try {
                        List<Empresa> empresas = empresaD.findAll();
                        request.setAttribute("empresas", empresas);
                    } catch (SQLException ex) {
                        Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    try {
                        List<Departamento> departamentos = departamentoD.findAll();
                        request.setAttribute("departamentos", departamentos);
                    } catch (SQLException ex) {
                        Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                    }
                                
                    request.setAttribute("action", "/Usuarios/registrar-empleado");
                    request.setAttribute("body", "/views/emp/AgregarP.jsp");
                    request.setAttribute("title", "Agregar Empleado");
                    getServletContext().getRequestDispatcher("/views/includes/MainInclude.jsp").forward(request, response);
                    break;
                case "/gestionar-empleado":
                    try {
                        if (empleadoD == null) {
                            empleadoD = new EmpleadoD();
                        }
                        if (empresaD == null) {
                            empresaD = new EmpresaD();
                        }                        
                        if (departamentoD == null) {
                            departamentoD = new DepartamentoD();
                        }
                        
                        List<Empleado> empleados = empleadoD.findAll();
                        List<Empresa> empresas = empresaD.findAll();
                        List<Departamento> departamentos = departamentoD.findAll();
                                                                                                
                        if (request.getParameter("empresa") != null && request.getParameter("dep") != null) {
                            if (!request.getParameter("empresa").equals("") && request.getParameter("dep").equals("")) {
                                Integer idE = Integer.parseInt(request.getParameter("empresa"));
                                empleados = empleadoD.findAllByE(idE);
                            }
                            if (request.getParameter("empresa").equals("") && !request.getParameter("dep").equals("")) {
                                Integer idD = Integer.parseInt(request.getParameter("dep"));
                                empleados = empleadoD.findAllByD(idD);
                            }
                            if (!request.getParameter("empresa").equals("") && !request.getParameter("dep").equals("")) {
                                Integer idE = Integer.parseInt(request.getParameter("empresa"));
                                Integer idD = Integer.parseInt(request.getParameter("dep"));
                                empleados = empleadoD.findAllByED(idE, idD);
                            }
                            if (request.getParameter("empresa").equals("") && request.getParameter("dep").equals("")) {
                                empleados = empleadoD.findAll();
                            }
                        }
                                                                                                     
                        request.setAttribute("empleados", empleados);
                        request.setAttribute("empresas", empresas);
                        request.setAttribute("departamentos", departamentos);
                        request.setAttribute("action", "/Usuarios/gestionar-empleado");
                    } catch (SQLException ex) {
                        Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                    }   
                    request.setAttribute("body", "/views/emp/GestionarP.jsp");
                    getServletContext().getRequestDispatcher("/views/includes/MainInclude.jsp").forward(request, response);
                    break;
                case "/editar-empleado":
                    if (empresaD == null) {
                        empresaD = new EmpresaD();
                    }
                    
                    if (departamentoD == null) {
                        departamentoD = new DepartamentoD();
                    }
                    
                    if (empleadoD == null) {
                        empleadoD = new EmpleadoD();
                    }
                    try {
                        List<Empresa> empresas = empresaD.findAll();
                        request.setAttribute("empresas", empresas);
                    } catch (SQLException ex) {
                        Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    try {
                        List<Departamento> departamentos = departamentoD.findAll();
                        request.setAttribute("departamentos", departamentos);
                    } catch (SQLException ex) {
                        Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    try {
                        empleado = empleadoD.findById(Integer.parseInt(request.getParameter("id")));
                        request.setAttribute("empleado", empleado);
                    } catch (SQLException ex) {
                        Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                    }                                                   
                    
                    request.setAttribute("action", "/Usuarios/editar-empleado");
                    request.setAttribute("body", "/views/emp/AgregarP.jsp");
                    request.setAttribute("title", "Editar Empleado");
                    request.setAttribute("info", true);
                    getServletContext().getRequestDispatcher("/views/includes/MainInclude.jsp").forward(request, response);
                    break; 
                case "/horario-empleado":
                    if (empleadoD == null) {
                        empleadoD = new EmpleadoD();
                    }
                    
                    try {
                        empleado = empleadoD.findById(Integer.parseInt(request.getParameter("id")));
                        request.setAttribute("empleado", empleado);
                    } catch (SQLException ex) {
                        Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                    } 
                    request.setAttribute("action", "/Usuarios/horario-empleado");
                    request.setAttribute("body", "/views/emp/Horario.jsp");
                    request.setAttribute("title", "Horario Empleado");                    
                    getServletContext().getRequestDispatcher("/views/includes/MainInclude.jsp").forward(request, response);
                    
                    break;
                default:
                    break;
            }        
        } else {
            response.sendRedirect("/Usuarios/login");
        }

    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        url = request.getServletPath();
        Empleado empleado = null;
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date fecha = null;
        Date fecha_i = null;
        
        switch (url) {
            case "/registrar-empleado":                
                {
                    String nombre = request.getParameter("nombre");
                    java.util.Date fecha_d = null;
                    try {
                        fecha_d = formato.parse(request.getParameter("fecha"));
                    } catch (ParseException ex) {
                        Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    java.sql.Date fecha_s = new java.sql.Date(fecha_d.getTime());                                       
                    java.util.Date fecha_d_1 = null;
                    try {
                        fecha_d_1 = formato.parse(request.getParameter("fecha_i"));
                    } catch (ParseException ex) {
                        Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    java.sql.Date fecha_s_1 = new java.sql.Date(fecha_d_1.getTime());                                                                                                      
                    String correo = request.getParameter("email");
                    String genero = request.getParameter("genero");
                    String telefono = request.getParameter("telefono");
                    String celular = request.getParameter("celular");                                        
                                                          
                    empleado = new Empleado();
                    if (!request.getParameter("idEmpr").equals("")) {
                        empleado.setEmpresaId(Integer.parseInt(request.getParameter("idEmpr")));
                    } else {
                        empleado.setEmpresaId(4);
                    }
                    
                    if (!request.getParameter("idDep").equals("")) {
                        empleado.setDepartamentoId(Integer.parseInt(request.getParameter("idDep")));
                    } else {
                        empleado.setDepartamentoId(5);
                    }
                    
                    empleado.setNombre(nombre);
                    empleado.setFecha(fecha_s);
                    empleado.setEmail(correo);
                    empleado.setGenero(genero);
                    empleado.setTelefono(telefono);
                    empleado.setCelular(celular);
                    empleado.setFechai(fecha_s_1);                    
                    
                    if (empleadoD == null) {
                        empleadoD = new EmpleadoD();
                    }
                    
                    try {
                        empleadoD.create(empleado);
                        response.sendRedirect("/Usuarios/gestionar-empleado");
                    } catch (SQLException ex) {
                        System.out.println(ex);
                        response.sendRedirect("/Usuarios/gestionar-empleado");
                        Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                    }                    
                    break;
                }
            case "/editar-empleado":
                {
                    if (empleadoD == null) {
                        empleadoD = new EmpleadoD();
                    }
                    
                    String nombre = request.getParameter("nombre");
                    java.util.Date fecha_d = null;
                    try {
                        fecha_d = formato.parse(request.getParameter("fecha"));
                    } catch (ParseException ex) {
                        Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    java.sql.Date fecha_s = new java.sql.Date(fecha_d.getTime());                    
                                                                                
                    String correo = request.getParameter("email");
                    String genero = request.getParameter("genero");
                    String telefono = request.getParameter("telefono");
                    String celular = request.getParameter("celular");                                        
                    
                    
                    try {
                        empleado = empleadoD.findById(Integer.parseInt(request.getParameter("idEmp")));
                    } catch (SQLException ex) {
                        Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    if (!request.getParameter("idEmpr").equals("")) {
                        System.out.println(request.getParameter("idEmpr"));
                        empleado.setEmpresaId(Integer.parseInt(request.getParameter("idEmpr")));
                    } else {
                        empleado.setEmpresaId(4);
                    }
                    
                    if (!request.getParameter("idDep").equals("")) {
                        empleado.setDepartamentoId(Integer.parseInt(request.getParameter("idDep")));
                    } else {
                        empleado.setDepartamentoId(5);
                    }
                    
                    empleado.setNombre(nombre);
                    empleado.setFecha(fecha_s);
                    empleado.setEmail(correo);
                    empleado.setGenero(genero);
                    empleado.setTelefono(telefono);
                    empleado.setCelular(celular);                                    
                                                            
                    try {
                        empleadoD.update(empleado);
                        response.sendRedirect("/Usuarios/gestionar-empleado");
                    } catch (SQLException ex) {
                        System.out.println(ex);
                        response.sendRedirect("/Usuarios/gestionar-empleado");
                        Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                    }                    
                    break;
                
                }
            case "/eliminar-empleado":
                {
                    String id = request.getParameter("idEmp");
                    if (empleadoD == null) {
                        empleadoD = new EmpleadoD();
                    }
                    try {
                        empleadoD.delete(Integer.parseInt(id));
                    } catch (SQLException ex) {
                        Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                
                }                                
                break;                 
                
            case "/horario-empleado":
                if (empleadoD == null) {
                    empleadoD = new EmpleadoD();
                }
                Time entrada = null;
                Time salida = null;
                DateFormat formatter = new SimpleDateFormat("HH:mm");
                try {
                    entrada = new java.sql.Time(formatter.parse(request.getParameter("entrada")).getTime());
                } catch (ParseException ex) {
                    Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    salida = new java.sql.Time(formatter.parse(request.getParameter("salida")).getTime());
                } catch (ParseException ex) {
                    Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    empleado = empleadoD.findById(Integer.parseInt(request.getParameter("idEmp")));
                } catch (SQLException ex) {
                    Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                }
                empleado.sethEntrada(entrada);
                empleado.sethSalida(salida);
                
                try {
                    empleadoD.Horarios(empleado);
                    response.sendRedirect("/Usuarios/gestionar-empleado");
                } catch (SQLException ex) {                        
                    response.sendRedirect("/Usuarios/gestionar-empleado");
                    Logger.getLogger(CtrlEmpleado.class.getName()).log(Level.SEVERE, null, ex);
                }                                                                 
                break;
            default:
                break;
        }
        
    }

}
