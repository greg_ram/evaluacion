<%-- 
    Document   : ContactoP
    Created on : 1/12/2019, 03:02:33 PM
    Author     : gregr
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3 bg-gray-900">
            <h6 class="m-0 font-weight-bold text-white" style="text-align: center">${title}</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="p-2">
                        <form id="form" action="${action}" method="POST">
                            <input type="hidden" value="${idProv}" name="idProv"/>
                            <input type="hidden" value="${contacto.idContacto}" name="idCon">
                            <div class="form-group row">
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" name="nombre"
                                           placeholder="Nombre" autocomplete="off" value="${contacto.nombreContacto}" required />
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <input name="email" class="form-control" type="email" 
                                           placeholder="Correo electrónico" autocomplete="off" value="${contacto.correo}" required />
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" name="telefono"
                                           placeholder="Teléfono" maxlength="10" autocomplete="off" value="${contacto.telefono}" required />
                                </div>                                    
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" name="ext"
                                           placeholder="Extensión" maxlength="120" autocomplete="off" value="${contacto.extension}" required />                                        
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" name="celular"
                                           placeholder="Celular" maxlength="10" autocomplete="off" value="${contacto.celular}" required />                                        
                                </div>
                            </div>                           
                            <hr />                               
                            <div class="col-sm-4 col-xs-12" style="text-align: right">
                                <button class="btn btn-primary btn-user" type="submit">Guardar</button>
                                <button class="btn btn-danger btn-user" type="button" onclick="location.href='editar-proveedor?idProv=${idProv}'">Cancelar</button>
                            </div>  
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
