<%-- 
    Document   : AgregarU
    Created on : 21/11/2019, 04:47:31 PM
    Author     : gregr
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container-fluid">     
    <div class="card shadow mb-4">
        <div class="card-header py-3 bg-gray-900">
            <h6 class="m-0 font-weight-bold text-white" style="text-align: center">${title}</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="p-2">
                        
                        <form id="form" action="${action}" method="POST">
                            <input type="text" name="idUser" value="${usuario.id}" hidden>
                            <div class="form-group row">
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" id="nombre" name="nombre"
                                           placeholder="Nombre completo" autocomplete="off" value="${usuario.nombre}" required />
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <input id="password" name="password" class="form-control" type="password" 
                                           placeholder="Contraseña" value="${usuario.password}" required />
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <select id="tipo" name="tipo" class="form-control" required>
                                        <option value="">Selecciona un tipo de usuario:</option>
                                        <option value="1" <c:if test="${usuario.tipo eq 1}">selected</c:if>>Administrador</option>
                                        <option value="2" <c:if test="${usuario.tipo eq 2}">selected</c:if>>Usuario normal</option>                                        
                                    </select>
                                </div>
                            </div>                                                  
                            <hr />                                
                            <div style="text-align: right">
                                <button class="btn btn-primary btn-user" type="submit">Guardar</button>                                
                            </div>                      
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>
