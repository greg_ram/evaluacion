<%-- 
    Document   : GestionarC
    Created on : 22/11/2019, 06:36:33 PM
    Author     : gregr
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800">Cuentas</h1>
    <p class="mb-4">Información general de las cuentas por pagar.</p>
    <div class="card shadow mb-4">
        <div class="card-header py-3 bg-gray-800">
            <h6 class="m-0 font-weight-bold text-white" style="text-align: center">Cuentas</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr>
                            <th style="text-align: center;">ID</th>
                            <th style="text-align: center;">Proveedor</th>
                            <th style="text-align: center;">Total</th>
                            <th style="text-align: center;">Anticipo</th>
                            <th style="text-align: center;">Adeudo</th>
                            <th style="text-align: center;">Información</th>
                        </tr>
                    </thead>                  
                    <tbody>
                        <c:forEach var="datos" items="${cuentas}">
                            <tr>
                                <td style="text-align: center;">${datos[0]}</td>
                                <td style="text-align: center;">${datos[2]}</td>
                                <td style="text-align: center;">${datos[3]}</td>
                                <td style="text-align: center;">${datos[4]}</td>
                                <td style="text-align: center;">${datos[5]}</td>
                                <td style="text-align: center;">
                                    <a onclick="location.href='cuenta-proveedor?idProv=${datos[1]}'" class="btn btn-info btn-circle">
                                        <i class="fas fa-info" style="color: white"></i>
                                    </a>                        
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>    
</div>