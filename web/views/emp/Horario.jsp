<%-- 
    Document   : AgregarP
    Created on : 21/11/2019, 05:03:37 PM
    Author     : gregr
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container-fluid">
   
    <button type="button" onclick="location.href='gestionar-empleado'" class="btn btn-outline-secondary">
        &larr; Atrás
    </button>
    <hr/>    
    <div class="card shadow mb-4">
        <div class="card-header py-3 bg-gray-900">
            <h6 class="m-0 font-weight-bold text-white" style="text-align: center">${title}</h6>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="p-2">
                        <form id="form" action="${action}" method="POST">
                            <input type="hidden" value="${empleado.id}" name="idEmp"/>
                            <div class="form-group row">                                
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" value="${empleado.hEntrada}" id="entrada" name="entrada"
                                           placeholder="Hora entrada" autocomplete="off" onfocus="(this.type = 'time')" required />
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" value="${empleado.hSalida}" id="salida" name="salida"
                                           placeholder="Hora salida" autocomplete="off" onfocus="(this.type = 'time')" required />
                                </div>                                                                                          
                            </div>                            
                            <hr />                               
                            <div style="text-align: right">
                                <button class="btn btn-primary btn-user" type="submit">Guardar</button>
                            </div>  
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>   
</div>

