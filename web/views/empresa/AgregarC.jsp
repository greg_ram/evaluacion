<%-- 
    Document   : AgregarC
    Created on : 22/11/2019, 06:33:53 PM
    Author     : gregr
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container-fluid">
    <div class="card shadow mb-4">
        <div class="card-header py-3 bg-gray-900">
            <h6 class="m-0 font-weight-bold text-white" style="text-align: center">${title}</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="p-2">
                        <form id="form" action="/Cuentas/agregar-cuenta" method="POST">
                            <div class="form-group row">
                                <div class="col-sm-4 col-xs-12">
                                    <c:if test="${idProv != null}"><input type="hidden" value="${idProv}" name="idProv"/></c:if>
                                    <select id="idProv" name="idProv" class="form-control" required <c:if test="${idProv != null}">disabled</c:if> >
                                        <option value="">Seleccione un proveedor:</option>
                                        <c:forEach var="values" items="${provs}">
                                            <option value="${values.idProveedor}" <c:if test="${values.idProveedor eq idProv}">selected</c:if> >${values.nombreProveedor}</option>
                                        </c:forEach>                                    
                                    </select>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" id="fecha" name="fecha"
                                           placeholder="Fecha de facturación" autocomplete="off" onfocus="(this.type = 'date')" required />
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <input id="email" name="factura" class="form-control" type="factura" 
                                           placeholder="No. Factura" autocomplete="off" required />
                                </div>                              
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4 col-xs-12">
                                    <select name="idAdq" class="form-control" required>
                                        <option>Seleccione un tipo de adquisición</option>
                                        <c:forEach var="values" items="${adqs}">
                                            <option value="${values.idAdquisicion}">${values.tpAdquisicion}</option>
                                        </c:forEach>
                                    </select>                                        
                                </div>     
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" id="desc" name="desc"
                                           placeholder="Descripción" autocomplete="off" required />
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <select name="camion" class="form-control">
                                        <option>Seleccione un camión</option>
                                        <c:forEach var="values" items="${camiones}">
                                            <option value="${values.idCamion}">${values.placas}</option>
                                        </c:forEach>
                                    </select>                                      
                                </div>                 
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" id="monto" name="monto"
                                           placeholder="Monto" autocomplete="off" required />                                        
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" name="adeudo" 
                                           placeholder="Adeudo" autocomplete="off" required/>
                                </div>
                                <div class="col-sm-4 col-xs-12">
                                    <input type="text" class="form-control" name="fecha_v" 
                                           placeholder="Fecha de vencimiento" onfocus="(this.type = 'date')" 
                                           autocomplete="off" required/>
                                </div>    
                            </div>
                            <hr />                                
                            <div style="text-align: right"> 
                                <button class="btn btn-primary btn-user" type="submit">Guardar</button>
                                <button class="btn btn-danger btn-user" type="button"
                                <c:choose>
                                    <c:when test="${idProv eq null}">onclick="location.href='cuentas'"</c:when>
                                    <c:when test="${idProv != null}">onclick="location.href='cuenta-proveedor?idProv=${idProv}'"</c:when>
                                </c:choose> >
                                Cancelar
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>