/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.dao.UsuarioD;
import model.dto.Usuario;;
/**
 *
 * @author gregr
 */
@WebServlet(name = "CtrlLogin", urlPatterns = {"/login", "/logout"})
public class CtrlLogin extends HttpServlet {
    
    private HttpSession session;

    private UsuarioD usuarioD;
    
    String url = "";
   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        url = request.getServletPath();
        
        if (url.equals("/login")) {
            getServletContext().getRequestDispatcher("/views/login.jsp").forward(request, response);
        } else if (url.equals("/logout")) {
            this.session = request.getSession();
            if (this.session != null) {
                this.session.invalidate();
            }
            response.sendRedirect("/Usuarios/login");
        }
     
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        url = request.getServletPath();
        
        if (url.equals("/login")) {
            String usuario = request.getParameter("usuario");
            String password = request.getParameter("password");                                  
            Usuario datos = new Usuario();
            datos.setNombre(usuario);
            datos.setPassword(password);
            
            if (usuarioD == null) {
                usuarioD = new UsuarioD();
            }
            
            try {                
                Usuario user = usuarioD.login(datos);                
                if (user != null) {                       
                    session = request.getSession(true);
                    if (user.getTipo() == 1) {
                        session.setAttribute("tipo", 1);                        
                    } else if (user.getTipo() == 2 ) {
                        session.setAttribute("tipo", 2);
                        
                    }                                                            
                    response.sendRedirect("/Usuarios/inicio");                                    
                } else {
                    response.sendRedirect("/Usuarios/login");                      
                }
                
            } catch (SQLException ex) {
                response.sendRedirect("/Usuarios/login");  
                Logger.getLogger(CtrlLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (url.equals("/logout")) {
            this.session = request.getSession();

            if (this.session != null) {
                this.session.invalidate();
            }
            response.sendRedirect("/Usuarios/login");

        }
    }

}
