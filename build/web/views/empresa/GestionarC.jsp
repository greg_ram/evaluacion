<%-- 
    Document   : GestionarC
    Created on : 22/11/2019, 06:36:33 PM
    Author     : gregr
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800">Empresas</h1>
    <p class="mb-4">Información general de las empresas en el sistema.</p>
    <div class="card shadow mb-4">
        <div class="card-header py-3 bg-gray-800">
            <h6 class="m-0 font-weight-bold text-white" style="text-align: center">Empresas</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr>
                            <th style="text-align: center;">ID</th>
                            <th style="text-align: center;">Nombre</th>
                            <th style="text-align: center;">Departamentos</th>
                        </tr>
                    </thead>                  
                    <tbody>
                        <c:forEach var="datos" items="${empresas}">
                            <tr>
                                <td style="text-align: center;">${datos.id}</td>
                                <td style="text-align: center;">${datos.nombre}</td>
                                <td style="text-align: center;">
                                    <a onclick="location.href='gestionar-departamentos'" class="btn btn-info btn-circle">
                                        <i class="fas fa-info" style="color: white"></i>
                                    </a>                        
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>    
</div>