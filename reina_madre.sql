-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.35-log - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Volcando estructura de base de datos para reina_madre
CREATE DATABASE IF NOT EXISTS `reina_madre` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `reina_madre`;

-- Volcando estructura para tabla reina_madre.departamento
CREATE TABLE IF NOT EXISTS `departamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla reina_madre.departamento: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
INSERT INTO `departamento` (`id`, `nombre`) VALUES
	(1, 'Desarrollo'),
	(2, 'Recursos Humanos'),
	(3, 'Finanzas'),
	(4, 'Soporte'),
	(5, NULL);
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;

-- Volcando estructura para tabla reina_madre.empleado
CREATE TABLE IF NOT EXISTS `empleado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `genero` varchar(10) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `celular` varchar(10) DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `id_departamento` int(11) DEFAULT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `h_entrada` time DEFAULT NULL,
  `h_salida` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_empleado_departamento` (`id_departamento`),
  KEY `FK_empleado_empresa` (`id_empresa`),
  CONSTRAINT `FK_empleado_departamento` FOREIGN KEY (`id_departamento`) REFERENCES `departamento` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_empleado_empresa` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla reina_madre.empleado: ~8 rows (aproximadamente)
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` (`id`, `nombre`, `fecha_nacimiento`, `email`, `genero`, `telefono`, `celular`, `fecha_ingreso`, `id_departamento`, `id_empresa`, `h_entrada`, `h_salida`) VALUES
	(6, 'Ramón', '2021-07-30', 'F@J.COM', '', '', '', '2021-07-30', 2, 2, NULL, NULL),
	(7, 'Felipe', '2021-07-22', 'actua@G.COM', 'H', '0000000000', '3333333333', '2021-07-28', 2, 3, '07:00:00', '18:00:00'),
	(8, 'Juan', '2021-07-30', 'juan@gmail.com', 'H', '4420000000', '7120000000', '2021-07-30', 1, 1, NULL, NULL),
	(9, 'María', '2001-07-30', 'maria@gmail.com', 'M', '5132123000', '8513024860', '2021-07-30', 1, 2, NULL, NULL),
	(10, 'Maricela', '2020-06-15', 'maricela@hotmail.com', '', '', '', '2021-07-29', 3, 1, NULL, NULL),
	(11, 'Monica', '1997-07-29', 'moni@gmail.com', '', '', '', '2021-07-28', 1, 3, NULL, NULL),
	(12, 'Jaime', '1992-04-26', 'f@yahoo.com', 'H', '5555555555', '3333333333', '2014-07-14', 4, 1, NULL, NULL),
	(13, 'Manuel', '2021-07-13', 'manuel@m.com', 'H', '1232143232', '1561615106', '2021-07-28', 2, 2, '14:34:55', NULL);
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;

-- Volcando estructura para tabla reina_madre.empresa
CREATE TABLE IF NOT EXISTS `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla reina_madre.empresa: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` (`id`, `nombre`) VALUES
	(1, 'A'),
	(2, 'B'),
	(3, 'C'),
	(4, NULL);
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;

-- Volcando estructura para tabla reina_madre.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla reina_madre.usuario: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` (`id`, `nombre`, `password`, `tipo`) VALUES
	(1, 'admin', '123', 1),
	(3, 'USER', '1234', 2);
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
