/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.dao.DepartamentoD;
import model.dao.EmpresaD;
import model.dto.Departamento;
import model.dto.Empresa;

/**
 *
 * @author gregr
 */
@WebServlet(name = "CtrlEmpresa", urlPatterns = {"/gestionar-empresas", "/gestionar-departamentos"})
public class CtrlEmpresa extends HttpServlet {
    
    private HttpSession session;    
    private EmpresaD empresaD;
    private DepartamentoD departamentoD;
       
    String url = "";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        this.session = request.getSession();
        
        url = request.getServletPath();        
        
        if (session.getAttribute("tipo") != null) {
            if (url.equals("/gestionar-empresas")) {
                if (empresaD == null) {
                    empresaD = new EmpresaD();
                }
                
                try {
                    List<Empresa> empresas = empresaD.findAll();
                    request.setAttribute("empresas", empresas);
                } catch (SQLException ex) {
                    Logger.getLogger(CtrlEmpresa.class.getName()).log(Level.SEVERE, null, ex);
                }
                request.setAttribute("body", "/views/empresa/GestionarC.jsp");
                getServletContext().getRequestDispatcher("/views/includes/MainInclude.jsp").forward(request, response);
                
            } else if (url.equals("/gestionar-departamentos")) {
                if (departamentoD == null) {
                    departamentoD = new DepartamentoD();
                }
                
                try {
                    List<Departamento> departamentos = departamentoD.findAll();
                    request.setAttribute("departamentos", departamentos);
                } catch (SQLException ex) {
                    Logger.getLogger(CtrlEmpresa.class.getName()).log(Level.SEVERE, null, ex);
                }
                request.setAttribute("body", "/views/dep/GestionarD.jsp");
                getServletContext().getRequestDispatcher("/views/includes/MainInclude.jsp").forward(request, response);
                
            }                            
        } else {
            response.sendRedirect("/Usuarios/login");
        }

    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

}
