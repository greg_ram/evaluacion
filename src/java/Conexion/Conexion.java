package Conexion;

import java.sql.Connection;
import java.sql.DriverManager;

public class Conexion {

    private Connection con;
    private final String DB_USUARIO = "root";
    private final String DB_PASSWORD = "1234";
    private final String DB_NOMBRE = "reina_madre";
    private final int DB_PUERTO = 3306;

    public Conexion() {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            this.con = DriverManager.getConnection("jdbc:mysql://localhost:3306/reina_madre", "root", "1234");
        } catch (Exception e) {
            this.con = null;
            e.printStackTrace();
        }
    }

    public Connection getCon() {        
        return this.con;
    }
}
