<%-- 
    Document   : CuentaByProv
    Created on : 22/11/2019, 07:21:42 PM
    Author     : gregr
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container-fluid">
    <button type="button" onclick="location.href='cuentas'" class="btn btn-outline-secondary">
        &larr; Atrás
    </button>
    <hr/>
    <h1 class="h3 mb-2 text-gray-800">Cuentas de proveedor: ${idProv.nombreProveedor}</h1>
    <p class="mb-4">Información general de las cuentas por pagar al proveedor.</p>
    <div style="text-align: right">
        <a onclick="location.href='agregar-cuenta?idProv=${idProv.idProveedor}'" class="btn btn-success btn-icon-split">
            <span class="icon text-white-50">
                <i class="fas fa-check" style="color: white"></i>
            </span>
            <span class="text" style="color: white">Agregar nueva cuenta.</span>
        </a>
        <hr/>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3 bg-gray-800">
            <h6 class="m-0 font-weight-bold text-white" style="text-align: center">Cuentas</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr>
                            <th style="text-align: center;">Fecha</th>
                            <th style="text-align: center;">No. Factura</th>
                            <th style="text-align: center;">Descripción</th>
                            <th style="text-align: center;">Camión</th>
                            <th style="text-align: center;">Fecha de vencimiento</th>
                            <th style="text-align: center;">Total</th>
                            <th style="text-align: center;">Adeudo</th>
                            <th style="text-align: center;">Estatus</th>
                            <th style="text-align: center;">Información</th>
                        </tr>
                    </thead>                  
                    <tbody>
                        <c:forEach var="datos" items="${cuentas}">
                            <c:if test="${datos.idProveedor.idProveedor eq idProv.idProveedor}">
                                <tr>
                                    <td style="text-align: center;">${datos.fechaFactura}</td>
                                    <td style="text-align: center;">${datos.noFactura}</td>
                                    <td style="text-align: center;">${datos.descripcion}</td>
                                    <td style="text-align: center;">${datos.idCamion.placas}</td>
                                    <td style="text-align: center;">${datos.fechaVencimiento}</td>
                                    <td style="text-align: center;">${datos.monto}</td>
                                    <td style="text-align: center;">${datos.adeudo}</td>
                                    <td style="text-align: center;">${datos.estatus}</td>
                                    <td style="text-align: center;">
                                        <a onclick="location.href='info-cuenta?cuenta=${datos.idCuenta}'" class="btn btn-info btn-circle">
                                            <i class="fas fa-info" style="color: white"></i>
                                        </a>                       
                                    </td>
                                </tr>
                            </c:if>
                        </c:forEach>                                                                                                            
                    </tbody>
                </table>
            </div>
        </div>
    </div>    
</div>