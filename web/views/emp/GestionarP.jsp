<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800">Empleados</h1>
    <p class="mb-4">Información general de los empleados registrados en el sistema.</p>
    <div class="card shadow mb-4">
        <div class="card-header py-3 bg-gray-800">
            <h6 class="m-0 font-weight-bold text-white" style="text-align: center">Empleados</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-12">
                    <div class="p-2">
                        <form id="form" action="${action}" method="GET">
                            <div class="form-group row">
                                <div class="col-sm-3 col-xs-12">
                                    <select id="empresa" name="empresa" class="form-control" >
                                        <option value="">Buscar por empresa:</option>
                                        <c:forEach var="values" items="${empresas}">
                                            <option value="${values.id}" <c:if test="${values.id eq idProv}">selected</c:if> >${values.nombre}</option>
                                        </c:forEach>                                                              
                                    </select>
                                </div>
                                <div class="col-sm-3 col-xs-12">
                                    <select id="dep" name="dep" class="form-control" >
                                        <option value="">Buscar por departamento</option>
                                        <c:forEach var="values" items="${departamentos}">
                                            <option value="${values.id}" <c:if test="${values.id eq idProv}">selected</c:if> >${values.nombre}</option>
                                        </c:forEach>                                                                               
                                    </select>
                                </div>
                                <div>
                                    <div style="text-align: right">
                                        <button class="btn btn-primary btn-user" type="submit">Buscar</button>                                
                                    </div>                                         
                                </div>
                            </div>
                        
                        </form>
                    </div>                    
                </div>                
            </div>
            
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr>
                            <th style="text-align: center;">Nombre</th>
                            <th style="text-align: center;">Empresa</th>
                            <th style="text-align: center;">Departamento</th>  
                            <th style="text-align: center;">Acciones</th>
                        </tr>
                    </thead>                  
                    <tbody>                                     
                        <c:forEach var="datos" items="${empleados}">
                            <tr>
                                <td style="text-align: center;">${datos.nombre}</td>
                                <td style="text-align: center;">${datos.empresa.nombre}</td>
                                <td style="text-align: center;">${datos.departamento.nombre}</td>                                                                                       
                                <td style="text-align: center;">
                                    <a title="Información" onclick="location.href='editar-empleado?id=${datos.id}'" class="btn btn-info btn-circle">
                                        <i class="fas fa-info" style="color: white"></i>
                                    </a>
                                    <a title="Eliminar" class="btn btn-danger btn-circle eliminar" data-id="${datos.id}">
                                        <i class="fas fa-trash" style="color: white"></i>
                                    </a>
                                    <a title="Horarios" class="btn btn-warning btn-circle" onclick="location.href='horario-empleado?id=${datos.id}'">
                                        <i class="fas fa-book" style="color: white"></i>
                                    </a>
                                </td>
                            </tr>  
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>    
</div>

<script src="vendor/jquery/jquery.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function (){
        $(".eliminar").click(function (){
            idEmp = $(this).attr("data-id");
            value = confirm("¿Desea eliminar este empleado?");           
            if (value) {
                $.ajax({
                    url: "eliminar-empleado",
                    type: "POST",
                    data : {
                        idEmp : idEmp
                    },
                    success: function (data, textStatus, jqXHR) {
                        location.reload();
                    }               
                });
            }
        }); 
    });           
</script>