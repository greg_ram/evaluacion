/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.dao.UsuarioD;
import model.dto.Usuario;

/**
 *
 * @author gregr
 */
@WebServlet(name = "CtrlUsuario", urlPatterns = {"/registrar-usuario", "/gestionar-usuario", "/editar-usuario", "/eliminar-usuario"})
public class CtrlUsuario extends HttpServlet {
    
    private HttpSession session;
    private UsuarioD usuarioD;
       
    String url = "";
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        this.session = request.getSession();
        url = request.getServletPath();

        if (session.getAttribute("tipo") != null) {

            switch (url) {
                case "/registrar-usuario":
                    request.setAttribute("body", "/views/usuario/AgregarU.jsp");
                    request.setAttribute("title", "Agregar usuario");
                    request.setAttribute("action", "/Usuarios/registrar-usuario");
                    getServletContext().getRequestDispatcher("/views/includes/MainInclude.jsp").forward(request, response);
                    break;
                case "/gestionar-usuario":
                    if (usuarioD == null) {
                        usuarioD = new UsuarioD();
                    }
                    
                    List<Usuario> usuarios = null;
                    try {
                        usuarios = usuarioD.findAll();
                    } catch (SQLException ex) {
                        Logger.getLogger(CtrlUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    request.setAttribute("usuarios", usuarios);
                    request.setAttribute("body", "/views/usuario/GestionarU.jsp");
                    getServletContext().getRequestDispatcher("/views/includes/MainInclude.jsp").forward(request, response);
                    break;
                case "/editar-usuario":
                    
                    if (usuarioD == null) {
                        usuarioD = new UsuarioD();
                    }
                    Usuario user = null;
                    try {
                        user = usuarioD.findById(Integer.parseInt(request.getParameter("id")));
                    } catch (SQLException ex) {
                        Logger.getLogger(CtrlUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    request.setAttribute("usuario", user);
                    request.setAttribute("body", "/views/usuario/AgregarU.jsp");
                    request.setAttribute("title", "Editar usuario");
                    request.setAttribute("action", "/Usuarios/editar-usuario");
                    getServletContext().getRequestDispatcher("/views/includes/MainInclude.jsp").forward(request, response);
                    break;
                default:
                    break;
            }
        } else {
            response.sendRedirect("/Usuarios/login");
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        url = request.getServletPath();
        Usuario usuario = null;        
              
        switch (url) {
            case "/registrar-usuario":
                {
                    String nombre = request.getParameter("nombre");                    
                    String password = request.getParameter("password");
                    String tipo = request.getParameter("tipo");
                    usuario = new Usuario();
                    usuario.setNombre(nombre);                    
                    usuario.setPassword(password);
                    usuario.setTipo(Integer.parseInt(request.getParameter("tipo")));
                    if (usuarioD == null) {
                        usuarioD = new UsuarioD();
                    }
                    try {
                        usuarioD.create(usuario);
                        response.sendRedirect("/Usuarios/gestionar-usuario");
                    } catch (SQLException ex) {
                        response.sendRedirect("/Usuarios/gestionar-usuario");
                        Logger.getLogger(CtrlUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;                    
                }
            case "/editar-usuario":
                {
                    if (usuarioD == null) {
                        usuarioD = new UsuarioD();
                    }
                    String id = request.getParameter("idUser");
                    String nombre = request.getParameter("nombre");                    
                    String password = request.getParameter("password");
                    String tipo = request.getParameter("tipo");
                    try {
                        usuario = usuarioD.findById(Integer.parseInt(request.getParameter("idUser")));
                    } catch (SQLException ex) {
                        Logger.getLogger(CtrlUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    usuario.setNombre(nombre);                    
                    usuario.setPassword(password);
                    usuario.setTipo(Integer.parseInt(tipo));
                    try {
                        usuarioD.update(usuario);
                        response.sendRedirect("/Usuarios/gestionar-usuario");
                    } catch (SQLException ex) {
                        response.sendRedirect("/Usuarios/gestionar-usuario");
                        Logger.getLogger(CtrlUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }                    
                    break;
                }
            case "/eliminar-usuario":
                {
                    if (usuarioD == null) {
                        usuarioD = new UsuarioD();
                    }
                    String id = request.getParameter("idUser");
                    try {
                        usuarioD.delete(Integer.parseInt(id));
                    } catch (SQLException ex) {
                        Logger.getLogger(CtrlUsuario.class.getName()).log(Level.SEVERE, null, ex);
                    }
                                        
                    break;
                }
            default:
                break;           
        }
    }


}
