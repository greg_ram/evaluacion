<ul class="navbar-nav bg-gray-900 sidebar sidebar-dark accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="inicio">
        <div class="sidebar-brand-text">
            <hr/>
            Sistema de Gesti�n de Empleados
        </div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item active">
        <hr/>
        <a class="nav-link" href="inicio">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Inicio</span></a>
    </li>
    <hr class="sidebar-divider">

    <div class="sidebar-heading">
    </div>    
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseProvider" aria-expanded="true" aria-controls="collapseProvider">
            <i class="fas fa-fw fa-book"></i>
            <span>Empleados</span>
        </a>
        <div id="collapseProvider" class="collapse" aria-labelledby="headingProvider" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Acciones:</h6>
                <a class="collapse-item" href="registrar-empleado">Crear</a>
                <a class="collapse-item" href="gestionar-empleado">Gestionar</a>
            </div>

        </div>
    </li>     
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>